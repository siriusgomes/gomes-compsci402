//
//  CardSuit.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation


enum CardSuit {
    case SPADES
    case HEARTS
    case DIAMONDS
    case CLUBS
}