//
//  Player.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation


class Player: NSObject {
    
    private var name:String?
    private var cards:Set<Card>?
    private var playerType:Int? // 1 - Real Player. 2 - CPU Player.
    private var playerNumber:Int?
    
    init(name:String, playerType:Int, playerNumber:Int) {
        self.name = name
        self.playerType = playerType
        self.playerNumber = playerNumber
    }
    
    override var description:String {
        //return self.name! + " from team " + team;
        return "Player name \(self.name!) - cards: \(cards)"
    }
    
    func debugPlayer() {
        for card in cards! {
            let name = self.name
            let cards = card.description()
            print("Player \(name) - cards: \(cards)");
        }
    }
    
    //MARK: Getters and Setters
    func getName() -> String {
        return self.name!;
    }
    
    func setName(name:String) {
        self.name = name;
    }
    
    func getCards() -> Set<Card> {
        return self.cards!;
    }
    
    func setCards(cards:Set<Card>) {
        self.cards = cards;
    }
    
    func getPlayerNumber() -> Int {
        return self.playerNumber!
    }
    
    func getPlayerType() -> Int{
        return playerType!;
    }
    
    func setPlayerType(playerType:Int) {
        self.playerType = playerType;
    }
    
//    @Override
//    public JSONObject toJsonObject() {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put(Constants.JSON_PLAYER_NAME, name);
//            
//            JSONArray jsonArrayCards = new JSONArray();
//            
//            for (Card card : cards) {
//                jsonArrayCards.put(card.toJsonObject());
//            }
//            
//            jsonObject.put(Constants.JSON_CARDS, jsonArrayCards);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return jsonObject;
//    }
//    
//    @Override
//    public Player fromJsonObject(JSONObject jsonObject) {
//        Player player = null;
//        try {
//            player = new Player(jsonObject.getString(Constants.JSON_PLAYER_NAME),1);
//            List<Card> listCards = new ArrayList<Card>();
//            JSONArray jsonArrayCards = jsonObject.getJSONArray(Constants.JSON_CARDS);
//            
//            Card cardFactor = new Card();
//            
//            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(0)));
//            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(1)));
//            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(2)));
//            
//            player.setCards(listCards);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return player;
//    }
}