//
//  Table.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/14/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//
import UIKit
import Foundation

/**
 Extensions to make the set of cards shuffleable.
 */
extension CollectionType {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}
extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

class Table: NSObject {
    private var listCards:Set<Card>?
    var player1:Player?
    var player2:Player?
    private var turnedCard:Card?
    private var gameController:GameController?
    
    private var player1Points:Int
    private var player2Points:Int
    
    private var player1Card:Card?
    private var player2Card:Card?
    
    private var imageView1:UIImageView?
    private var imageView2:UIImageView?
    
    private var turnsWinner:[Int]?
    
    private var gameType:GameTypeEnum
    
    // Constructor used in .SinglePlayer and MultiPlayerServer modes.
    init(player1:Player, player2:Player, gameController:GameController, gameType:GameTypeEnum) {
        self.player1Points = 0
        self.player2Points = 0
        self.gameType = gameType
        super.init()
        self.gameController = gameController
        self.player1 = player1
        self.player2 = player2
        turnsWinner = [Int](count: 3, repeatedValue: -999)
        gameController.setLabelPointsText("Player1: \(player1Points)\nPlayer2: \(player2Points)")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didPlayerPlayedCard:", name: "cardPlayed", object: nil)
    }
    
    // Constructor used in JoinMultiPlayer mode.
    init(player2:Player, gameController:GameController, gameType:GameTypeEnum, cards:Set<Card>, turnedCard:Card) {
        self.player1Points = 0
        self.player2Points = 0
        self.gameType = gameType
        super.init()
        self.gameController = gameController
        self.player1 = nil
        self.player2 = player2
        turnsWinner = [Int](count: 3, repeatedValue: -999)
        self.player2?.setCards(cards)
        self.turnedCard = turnedCard
        gameController.setLabelPointsText("Player1: \(player1Points)\nPlayer2: \(player2Points)")
    }
    
    func getTurn() -> Int {
        var turn = 0
        
        if (turnsWinner![0] != -999) {
            turn = 1
        }
        
        if (turnsWinner![1] != -999) {
            turn = 2
        }
        return turn
    }
    
    func didPlayerPlayedCard(notification:NSNotification) {
        
        
//        // If it is a multiplayer game, we should notify the other device about the card played!
//        if (gameType == .MultiPlayerServer || gameType == .MultiPlayerClient) {
//            let test = NSMutableDictionary()
//            
//            if (gameType == .MultiPlayerServer)
//            test.setValue(Constants.SERVER_PLAY_CARD, forKey: Constants.PACKAGE_TYPE)
//            test.setValue(table!.getTurnedCard().description(), forKey: Constants.JSON_CARD_TURNED)
//            let cards = NSMutableArray()
//            for card in (table!.player2?.getCards())! {
//                cards.addObject(card.description())
//            }
//            test.setValue(cards, forKey: Constants.JSON_CARDS)
//            
//            do {
//                
//                let jsonData = try NSJSONSerialization.dataWithJSONObject(test, options: NSJSONWritingOptions.PrettyPrinted)
//                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
//                print(jsonString)
//                sendData(jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!)
//                
//                //            let msg = self.messageField.text.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
//                
//            }
//            catch _ {
//            }
//        }
        
        
        //gameController?.sendData(<#T##data: NSData##NSData#>)
        
        
        let card = notification.object as! Card!
        let player = notification.userInfo!["player"] as! Player!
        let cardImage = notification.userInfo!["UIImageViewCard"] as! UIImageView!
        
        print("Table", "didPlayerPlayedCard", "Player: \(player.getPlayerNumber()) - Name: \(player.getName())", "Card Played: \(card.description())", "cardImage:", cardImage)
        
        // If player one or two played card, let's set it in the Table..
        if (player.getPlayerNumber() == 1) {
            self.player1Card = card
            self.player1Card?.setCardImage(cardImage)
            imageView1 = cardImage
        }
        if (player.getPlayerNumber() == 2) {
            self.player2Card = card
            self.player2Card?.setCardImage(cardImage)
            imageView2 = cardImage
        }
        
        // Calculate what's the actual turn.
        var turn = 0

        if (turnsWinner![0] != -999) {
            turn = 1
        }
        
        if (turnsWinner![1] != -999) {
            turn = 2
        }
        
        // If we have cards from both players, let's compare their values and define who's the winner of the turn!
        if ((self.player1Card != nil) && (self.player2Card != nil)) {

            turnsWinner![turn] = Table.compareCards(self.player1Card!, c2: self.player2Card!, turned: turnedCard!)
            
            
            let roundWinner = verifyRoundWinner()
            
            if (roundWinner != 0) {

                if (roundWinner == 1) {
                    player1Points++;
                    print("there is a round winner!!", "P1")
                    //TODO avoid swipes from here.
                    gameController!.setLabelMessageText("Winner is: Player1")
                    gameController!.setLabelPointsText("Player1: \(player1Points)\nPlayer2: \(player2Points)")
                }
                else if (roundWinner == 2) {
                    player2Points++
                    print("there is a round winner!!", "P2")
                    gameController!.setLabelMessageText("Winner is: Player2")
                    gameController!.setLabelPointsText("Player1: \(player1Points)\nPlayer2: \(player2Points)")
                }
                //startGame()
                performSelector("startNewRound:", withObject: roundWinner, afterDelay: 2.0)
                //TODO fazer limpar as cartas / enviar placar
//                if (roundWinner == 2) {
//                    print("Scheduling call to CPU to Play")
//                    performSelector("callCpuToPlay", withObject: nil, afterDelay: 1)
//                }
            }
            else {
                //NSTimer.scheduledTimerWithTimeInterval(0.75, target: self, selector: "removeCardsFromScreen", userInfo: nil, repeats: false)

                // If the winner of the turn is the CPU (-1)
                if (gameType == .SinglePlayer) {
                    performSelector("removeCardsFromScreen", withObject: nil, afterDelay: 0.75)
                    if (turnsWinner![turn] == 2) {
                        // Notify CPU to play..
                        //print("Scheduling call to CPU to Play")
                        performSelector("callCpuToPlay", withObject: nil, afterDelay: 1.5)
                        
                    }
                }
                else if (gameType == .MultiPlayerServer) {
                    
                    //Clean the screen for the server and the client.
                    
                    //PERFORM SELECTOR WASN'T WORKING HERE, I DON'T KNOW WHY.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.75 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                        self.player1Card?.getCardImage().hidden = true
                        self.player2Card?.getCardImage().hidden = true
                        self.imageView1?.hidden = true
                        self.imageView2?.hidden = true
                        self.gameController!.view.setNeedsLayout()
                    }
                    
                    //Client
                    let cleanScreenPackage = NSMutableDictionary()
                    cleanScreenPackage.setValue(Constants.CLEAN_SCREEN, forKey: Constants.PACKAGE_TYPE)
                    print("Sending package:",cleanScreenPackage)
                    gameController!.sendData(cleanScreenPackage)
                }
            }
            
        }
        else {
            if (gameType == .SinglePlayer) {
                if (player.getPlayerNumber() == 1) {
                    // Notify CPU to play..
                    //print("Scheduling call to CPU to Play")
                    performSelector("callCpuToPlay", withObject: nil, afterDelay: 1.5)
                    
                }
            }
        }
    }

    func callCpuToPlay() {
        //print("Calling CPU to Play")
        NSNotificationCenter.defaultCenter().postNotificationName("cpuNeedToPlay", object: nil)
    }
    
    func removeCardsFromScreen() {
        print("Removing cards from screen:", self.player1Card?.getCardImage(), self.player2Card?.getCardImage())
        print("Removing cards from screen:", self.player1Card?.description(), player2Card?.description())
        print("Removing cards from screen:", imageView1,imageView2)

       // dispatch_async(dispatch_get_main_queue(), {
            self.player1Card?.getCardImage().hidden = true
            self.player2Card?.getCardImage().hidden = true
            self.imageView1?.hidden = true
            self.imageView2?.hidden = true
            self.gameController!.view.setNeedsLayout()
        //})
       
        self.player1Card = nil
        self.player2Card = nil
    }
    
    func startNewRound(winner:AnyObject) {
        print("Starting new round")
        
        if (gameType == .SinglePlayer) {
            gameController!.startSinglePlayer()
            if ((winner as! Int) == 2){
                print("Scheduling call to CPU to Play")
                performSelector("callCpuToPlay", withObject: nil, afterDelay: 1.5)
            }
        }
        else if (gameType == .MultiPlayerServer) {
            gameController!.startMultiplayerPlayer(player1Points, scoreTeam2: player2Points)
        }
        gameController!.setLabelMessageText("")
        //TODO restart swipes from here.
    }
    
    /**
    * Function that will verify if there is a winner of the round.
    * It will return 0 if there is no winner.
    * Or it will return 1 or 2 in case of team 1 or team 2 victory, respectively. */
    func verifyRoundWinner() -> Int{
        var winnerTeamNumber:Int;
        
        // if first two are != -999, so we can already define a winner for the round...
        if (turnsWinner![0] != -999 && turnsWinner![1] != -999) {
            
            // if the winner of the first two turns is the same, and it's not tie, return this guy
            // if the first turn has a winner, and the second turn ties, return this guy too.
            if ((turnsWinner![0] == turnsWinner![1] && turnsWinner![0] != 0) || (turnsWinner![0] != 0 && turnsWinner![1] == 0)) {
                winnerTeamNumber = turnsWinner![0];
            }
                // if the first turn ties and the second turn has a winner, return this guy.
            else if ((turnsWinner![0] == 0 && turnsWinner![1] != 0)) {
                winnerTeamNumber = turnsWinner![1];
            }
            else {
                if (turnsWinner![2] != -999) {
                    if (turnsWinner![2] == 0) {
                        winnerTeamNumber = turnsWinner![0];
                    }
                    else {
                        winnerTeamNumber = turnsWinner![2];
                    }
                }
                else {
                    winnerTeamNumber = 0;
                }
            }
        }
        else {
            winnerTeamNumber = 0;
        }
        return winnerTeamNumber;
    }
    
    
    
    /**
    * This method will start the game:
    * 1-Shuffling the cards
    * 2-Giving the cards for the players in both teams
    * 3-Turning one card*/
    //todo some animations here would be nice...
    func startGame() {
        print("Table", "Starting new Game");
        
        turnsWinner = [Int](count: 3, repeatedValue: -999);
        self.player1Card = nil
        self.player2Card = nil
        
        //GameRules.listRound = new ArrayList<Round>();
        
        //listCards!.removeAll();
        shuffleCards()
        print("Table", "Cards shuffled")

        player1!.setCards(get3Cards(false))
        player1!.debugPlayer()

        player2!.setCards(get3Cards(true))
        player2!.debugPlayer()

        turnedCard = (getRandomCard())
        print("Table", "Card turned is: \(turnedCard?.description())")
        //setScoreOnScreen();
    }
    
    /**
     * Function that will compare two cards.
     * The expected return values are:
     * 1 if c1 > c2
     * 0 if c1 == c2
     * 2 if c1 < c2
     * */
    static func compareCards(c1:Card, c2:Card, turned:Card) -> Int {
        var manilhaValue = turned.getValue()+1;
        if (manilhaValue == 11) { // Means that the turned card was a 3, so the manilha is 4, which value is 1.
            manilhaValue = 1;
        }
        
        // This means c1 is Manilha, so let's set the new value for c1
        if (manilhaValue == c1.getValue()) {
            c1.setValue(10 + getSuitValue(c1));
        }
        
        // This means c2 is Manilha, so let's set the new value for c2
        if (manilhaValue == c2.getValue()) {
            c2.setValue(10 + getSuitValue(c2));
        }
        
        print("CompareCards", "Player 1 card is ", c1.description(), " - value is: ", c1.getValue());
        print("CompareCards", "Player 2 card is ", c2.description(), " - value is: ", c2.getValue());
        
        if (c1.getValue() < c2.getValue()) {
            return 2;
        }
        else if (c1.getValue() > c2.getValue()) {
            return 1;
        }
        else {//(c1.getValue() == c2.getValue()) {
            return 0;
        }
    }
    
    /**
    * Function that will return a value for the suit of the card
    * Spades   = '♠'  - 2
    * Hearts   = '♥'  - 3
    * Diamonds = '♦'  - 1
    * Clubs    = '♣'  - 4
    */
    static func getSuitValue(card:Card) -> Int{
        if (card.getSuit() == CardSuit.CLUBS) {
            return 4;
        }
        else if (card.getSuit() == CardSuit.HEARTS) {
            return 3;
        }
        else if (card.getSuit() == CardSuit.SPADES) {
            return 2;
        }
        else if (card.getSuit() == CardSuit.DIAMONDS) {
            return 1;
        }
        return 0;
    }
    
    /**
     Function to return the card number "card" of the player "player". */
    func getCardPlayer(player:Int, card:Int) -> Card {
        var cardReturn:Card? = nil
        if (player == 1) {
            cardReturn = (player1?.getCards()[(player1?.getCards().startIndex.advancedBy(card-1))!])!
        }
        else { //if (player == 2) {
            cardReturn = (player2?.getCards()[(player2?.getCards().startIndex.advancedBy(card-1))!])!
        }
        
        return cardReturn!
    }
    
    /**
     Function to return the turned card.
     */
    func getTurnedCard() -> Card {
        return self.turnedCard!
    }
    
    /**
    * Method that return three cards from the listCards.
    * */
    private func get3Cards(isOpponent:Bool) -> Set<Card> {
        var listCard:Set<Card> = Set<Card>();
        
        let card1 = getRandomCard()
        let card2 = getRandomCard()
        let card3 = getRandomCard()
        
        if (isOpponent) {
            card1.setCardImage((gameController?.opponentPlayerCard1)!)
            card2.setCardImage((gameController?.opponentPlayerCard2)!)
            card3.setCardImage((gameController?.opponentPlayerCard3)!)
        }
        else {
            card1.setCardImage((gameController?.localPlayerCard1)!)
            card2.setCardImage((gameController?.localPlayerCard2)!)
            card3.setCardImage((gameController?.localPlayerCard3)!)
        }
        listCard.insert(card1);
        listCard.insert(card2);
        listCard.insert(card3);
        return listCard;
    }
    
    private func getRandomCard() -> Card {
        let randomNumber = (Int)(arc4random() % (UInt32)((listCards?.count)!-1))
        return (listCards?.removeAtIndex((listCards?.startIndex.advancedBy(randomNumber))!))!
    }
    /**
     * Method that add all the valid Truco cards to the listCards, and shuffle the listCards.
     * */
    private func shuffleCards() {
        
        listCards = Set<Card>();
        listCards?.insert(Card(id: CardValue.ACE, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.ACE, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.ACE, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.ACE, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.TWO, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.TWO, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.TWO, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.TWO, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.THREE, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.THREE, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.THREE, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.THREE, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.FOUR, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.FOUR, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.FOUR, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.FOUR, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.FIVE, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.FIVE, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.FIVE, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.FIVE, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.SIX, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.SIX, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.SIX, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.SIX, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.SEVEN, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.SEVEN, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.SEVEN, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.SEVEN, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.QUEEN, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.QUEEN, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.QUEEN, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.QUEEN, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.JACK, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.JACK, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.JACK, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.JACK, suit: CardSuit.CLUBS));
        
        listCards?.insert(Card(id: CardValue.KING, suit: CardSuit.DIAMONDS));
        listCards?.insert(Card(id: CardValue.KING, suit: CardSuit.SPADES));
        listCards?.insert(Card(id: CardValue.KING, suit: CardSuit.HEARTS));
        listCards?.insert(Card(id: CardValue.KING, suit: CardSuit.CLUBS));
        
        listCards?.shuffle();
        // It kinda doesn't work.. it always shuffle in the same way, o.O

    }

}