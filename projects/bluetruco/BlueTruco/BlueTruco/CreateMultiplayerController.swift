//
//  CreateMultiplayerController.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/22/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

//class CreateMultiplayerControllerr: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate {
//    
//    static var peerID: MCPeerID!
//    static var mcSession: MCSession!
//    static var mcAdvertiserAssistant: MCAdvertiserAssistant!
//    
//    @IBOutlet weak var buttonCreateMultiplayerGame: UIButton!
//
//    @IBAction func onStartMultiplayer(sender: AnyObject) {
//        GameController.gameMode = 3
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        CreateMultiplayerController.peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
//        CreateMultiplayerController.mcSession = MCSession(peer: CreateMultiplayerController.peerID, securityIdentity: nil, encryptionPreference: .Required)
//        CreateMultiplayerController.mcSession.delegate = self
//
//        print("CreatingMultiplayerGame", CreateMultiplayerController.peerID)
//        
//        startHosting(nil)
//        //joinSession(nil)
//    }
//    
//    
//    func sendData(data: NSData) {
//        if CreateMultiplayerController.mcSession.connectedPeers.count > 0 {
//            do {
//                try CreateMultiplayerController.mcSession.sendData(data, toPeers: CreateMultiplayerController.mcSession.connectedPeers, withMode: .Reliable)
//            } catch let error as NSError {
//                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .Alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//                presentViewController(ac, animated: true, completion: nil)
//            }
//
//        }
//    }
//    
//    func startHosting(action: UIAlertAction!) {
//        CreateMultiplayerController.mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: CreateMultiplayerController.mcSession)
//        CreateMultiplayerController.mcAdvertiserAssistant.start()
//    }
//    
//    func joinSession(action: UIAlertAction!) {
//        let mcBrowser = MCBrowserViewController(serviceType: "hws-kb", session: CreateMultiplayerController.mcSession)
//        mcBrowser.delegate = self
//        presentViewController(mcBrowser, animated: true, completion: nil)
//    }
//    
//    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveStream stream: \(stream), withName streamName: \(streamName), fromPeer peerID: \(peerID))")
//    }
//    
//    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
//        print("func session(session: \(session), didStartReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), withProgress progress: \(progress))")
//    }
//    
//    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
//        print("func session(session: \(session), didFinishReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), atURL localURL: \(localURL), withError error: \(error!))")
//    }
//    
//    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveData data: \(data), fromPeer peerID: \(peerID))")
//        
//        NSNotificationCenter.defaultCenter().postNotificationName("receivedPackage", object: data, userInfo: nil)
//    }
//
//    
//    
//    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerDidFinish(browserViewController: \(browserViewController))")
//    }
//    
//    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerWasCancelled(browserViewController: \(browserViewController))")
//    }
//    
//    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
//        print("func session(session: \(session), peer peerID: \(peerID), didChangeState state: \(state))")
//        switch state {
//        case MCSessionState.Connected:
//            print("Connected: \(peerID.displayName)")
//            buttonCreateMultiplayerGame.enabled = true
//            self.view.setNeedsDisplay()
//            
//        case MCSessionState.Connecting:
//            print("Connecting: \(peerID.displayName)")
//            
//        case MCSessionState.NotConnected:
//            print("Not Connected: \(peerID.displayName)")
//        }
//    }
//    
//}