//
//  Card.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation
import UIKit


//protocol Hashable : Equatable {
//    var hashValue: Int { get }
//}


func ==(lhs:Card, rhs:Card) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

class Card: Hashable, Equatable {
    
    private var id:CardValue;
    private var suit:CardSuit;
    private var cardImage:UIImageView?;
    private var realImageCard:UIImage?;
    private var value:Int;
   
    
    var hashValue: Int {
        get {
            return "\(self.id),\(self.suit)".hashValue
        }
    }
    
    //MARK: Constructors
    init(id:CardValue, suit:CardSuit) {
        self.id = id
        self.suit = suit;
        self.value = 0;
        setValue();
    }
    
    init(idsuit:String) {
        let id = idsuit[idsuit.startIndex.advancedBy(0)]
        let suit = idsuit[idsuit.startIndex.advancedBy(1)]
        if (id == "A") {
            self.id = .ACE
        }
        else if (id == "2") {
            self.id = .TWO
        }
        else if (id == "3") {
            self.id = .THREE
        }
        else if (id == "4") {
            self.id = .FOUR
        }
        else if (id == "5") {
            self.id = .FIVE
        }
        else if (id == "6") {
            self.id = .SIX
        }
        else if (id == "7") {
            self.id = .SEVEN
        }
        else if (id == "Q") {
            self.id = .QUEEN
        }
        else if (id == "J") {
            self.id = .JACK
        }
        else {//if (id == "K") {
            self.id = .KING
        }


        if (suit == "S") {
            self.suit = .SPADES
        }
        else if (suit == "C") {
            self.suit = .HEARTS
        }
        else if (suit == "O") {
            self.suit = .DIAMONDS
        }
        else {//if (suit == "♣") {
            self.suit = .CLUBS
        }
        self.value = 0
        setValue()
    }
    
    /** 
     Sets the value of the card according to the id. 
     */
    private func setValue() {
        switch (id) {
        case .ACE:
            value = 8;
            break;
        case .TWO:
            value = 9;
            break;
        case .THREE:
            value = 10;
            break;
        case .FOUR:
            value = 1;
            break;
        case .FIVE:
            value = 2;
            break;
        case .SIX:
            value = 3;
            break;
        case .SEVEN:
            value = 4;
            break;
        case .QUEEN:
            value = 5;
            break;
        case .JACK:
            value = 6;
            break;
        case .KING:
            value = 7;
            break;
        default:
            value = 0;
        }
    }
    
    
    func description() -> String {
        var card:String  = "";
        switch (id) {
        case .ACE:
            card += "A";
            break;
        case .TWO:
            card += "2";
            break;
        case .THREE:
            card += "3";
            break;
        case .FOUR:
            card += "4";
            break;
        case .FIVE:
            card += "5";
            break;
        case .SIX:
            card += "6";
            break;
        case .SEVEN:
            card += "7";
            break;
        case .QUEEN:
            card += "Q";
            break;
        case .JACK:
            card += "J";
            break;
        case .KING:
            card += "K";
            break;
        case .GENERIC:
            card += "Generic";
        }
        
        switch (suit) {
        case .SPADES:
            card += "S";//"♠";
            break;
        case .HEARTS:
            card += "C";//"♥";
            break;
        case .DIAMONDS:
            card += "O";//"♦"
            break;
        case .CLUBS:
            card += "Z";//"♣"
            break;
        }
        return card;
    }
    
    
    //MARK: Getters and Setters
    func getId() -> CardValue {
        return self.id
    }
    
    func setId(id:CardValue) {
        self.id = id
    }
    
    func getSuit() -> CardSuit {
        return self.suit
    }
    
    func setSuit(suit:CardSuit) {
        self.suit = suit
    }
    
    func getValue() -> Int {
        return self.value
    }
    
    func setValue(value:Int) {
        self.value = value
    }
    
    func getCardImage() -> UIImageView {
        return self.cardImage!
    }
    
    func setCardImage(cardImage:UIImageView) {
        self.cardImage = cardImage
    }

    func getRealCardImage() -> UIImage {
        return self.realImageCard!
    }
    
    func setRealCardImage(realImageCard:UIImage) {
        self.realImageCard = realImageCard
    }
}