//
//  CpuAI.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/20/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation


class CpuAI: NSObject {
    
    let table:Table?
    
    private var cardToPlay:Int = 1
    
    init(table:Table) {
        self.table = table
        super.init()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "cpuNeedToPlay:", name: "cpuNeedToPlay", object: nil)
    }
    
    func cpuNeedToPlay(notification:NSNotification) {
//        if (Table.player1Card != nil) {
//            Table.compareCards(Table.player1Card, c2: table?.getCardPlayer(2, card: 1), turned: <#T##Card#>)
//        }
        NSNotificationCenter.defaultCenter().postNotificationName("cpuPlayedCard", object: nil, userInfo: ["card": cardToPlay])
        if (cardToPlay==1) {
            cardToPlay=2
        }
        else if (cardToPlay==2) {
            cardToPlay=3
        }
        else if (cardToPlay==3) {
            cardToPlay=1
        }
    }
    
    func reset() {
        self.cardToPlay = 1
    }
    
}