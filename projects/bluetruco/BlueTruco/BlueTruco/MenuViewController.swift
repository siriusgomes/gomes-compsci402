//
//  ViewController.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bounds = UIScreen.mainScreen().bounds
        let width = bounds.size.width
        let height = bounds.size.height
        print("Screen bounds, width and height:", bounds, width, height)

        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    @IBAction func onSinglePlayer(sender: AnyObject) {
        GameController.gameMode = 1
    }
    @IBAction func onCreateMultiPlayer(sender: AnyObject) {
        GameController.gameMode = 3
    }
    @IBAction func onJoinMultiPlayer(sender: AnyObject) {
        GameController.gameMode = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}

