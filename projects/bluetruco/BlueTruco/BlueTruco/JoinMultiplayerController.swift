//
//  JoinMultiplayerController.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/22/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

//class JoinMultiplayerController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate {
//    
//    static var peerID: MCPeerID!
//    static var mcSession: MCSession!
//    static var mcAdvertiserAssistant: MCAdvertiserAssistant!
//    
//    @IBOutlet weak var buttonJoinMultiplayerGame: UIButton!
//
//    @IBAction func onJoinMultiplayer(sender: AnyObject) {
//        GameController.gameMode = 2
//    }
//    
//    @IBOutlet var onJoinMultiplayer: NSObject!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    
//        
//        JoinMultiplayerController.peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
//        JoinMultiplayerController.mcSession = MCSession(peer: JoinMultiplayerController.peerID, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.Required)
//        JoinMultiplayerController.mcSession.delegate = self
//        
//        print("JoiningMultiplayerGame", JoinMultiplayerController.peerID)
//        
//        //startHosting(nil)
//        joinSession(nil)
//    }
//    
//    
//    func sendData(data: NSData) {
//        if JoinMultiplayerController.mcSession.connectedPeers.count > 0 {
//            do {
//                try JoinMultiplayerController.mcSession.sendData(data, toPeers: JoinMultiplayerController.mcSession.connectedPeers, withMode: .Reliable)
//            } catch let error as NSError {
//                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .Alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//                presentViewController(ac, animated: true, completion: nil)
//            }
//            
//        }
//    }
//    
//    func startHosting(action: UIAlertAction!) {
//        JoinMultiplayerController.mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: JoinMultiplayerController.mcSession)
//        JoinMultiplayerController.mcAdvertiserAssistant.start()
//    }
//    
//    func joinSession(action: UIAlertAction!) {
//        let mcBrowser = MCBrowserViewController(serviceType: "hws-kb", session: JoinMultiplayerController.mcSession)
//        mcBrowser.delegate = self
//        presentViewController(mcBrowser, animated: true, completion: nil)
//    }
//    
//    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveStream stream: \(stream), withName streamName: \(streamName), fromPeer peerID: \(peerID))")
//    }
//    
//    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
//        print("func session(session: \(session), didStartReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), withProgress progress: \(progress))")
//    }
//    
//    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
//        print("func session(session: \(session), didFinishReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), atURL localURL: \(localURL), withError error: \(error!))")
//    }
//    
//    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveData data: \(data), fromPeer peerID: \(peerID))")
//        print(data)
//        print(data.classForCoder)
//        print(data.description)
//        print(data.superclass)
//        
//        let dictionary: [String: AnyObject] = ["data": data, "fromPeer": peerID]
//        NSNotificationCenter.defaultCenter().postNotificationName("receivedPackage", object: dictionary, userInfo: nil)
//    }
//    
//    
//    
//    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerDidFinish(browserViewController: \(browserViewController))")
//    }
//    
//    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerWasCancelled(browserViewController: \(browserViewController))")
//    }
//    
//    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
//        print("func session(session: \(session), peer peerID: \(peerID), didChangeState state: \(state))")
//        switch state {
//        case MCSessionState.Connected:
//            print("Connected: \(peerID.displayName)")
//            buttonJoinMultiplayerGame.enabled = true
//            self.view.setNeedsDisplay()
//            
//        case MCSessionState.Connecting:
//            print("Connecting: \(peerID.displayName)")
//            
//        case MCSessionState.NotConnected:
//            print("Not Connected: \(peerID.displayName)")
//        }
//    }
//    
//}