//
//  CardValue.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation

enum CardValue {
    case ACE
    case TWO
    case THREE
    case FOUR
    case FIVE
    case SIX
    case SEVEN
    case QUEEN
    case JACK
    case KING
    case GENERIC
}