//
//  Round.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/14/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation

func ==(lhs:Round, rhs:Round) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

class Round: Hashable, Equatable {
    private var player:Player
    private var card:Card
    
    init(player:Player, card:Card) {
        self.player = player;
        self.card = card;
    }
    
    var hashValue: Int {
        get {
            return "\(self.player.hashValue),\(self.card.hashValue)".hashValue
        }
    }
    
    func getPlayer() -> Player{
        return self.player;
    }
    
    func setPlayer(player:Player) {
        self.player = player;
    }
    
    func getCard() -> Card {
        return card;
    }
    
    func setCard(card:Card) {
        self.card = card;
    }
}