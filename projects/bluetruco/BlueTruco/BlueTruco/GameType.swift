//
//  GameType.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/22/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation

enum GameTypeEnum {
    case SinglePlayer
    case MultiPlayerServer
    case MultiPlayerClient
}