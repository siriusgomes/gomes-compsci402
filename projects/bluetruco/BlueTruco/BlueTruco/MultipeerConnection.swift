////
////  MultipeerConnection.swift
////  BlueTruco
////
////  Created by Sírius Gomes on 11/22/15.
////  Copyright © 2015 Sírius Gomes. All rights reserved.
////
//
//import Foundation
//import MultipeerConnectivity
//
//class MultipeerConnection: MCSessionDelegate, MCBrowserViewControllerDelegate {
//    
//    var peerID: MCPeerID!
//    var mcSession: MCSession!
//    var mcAdvertiserAssistant: MCAdvertiserAssistant!
//
//    
//    //    func sendText(str: String) {
//    //        if mcSession.connectedPeers.count > 0 {
//    //
//    //            do {
//    //                try mcSession.sendData(str.dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: mcSession.connectedPeers, withMode: .Reliable)
//    //            } catch let error as NSError {
//    //                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .Alert)
//    //                ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//    //                presentViewController(ac, animated: true, completion: nil)
//    //            }
//    //
//    //        }
//    //    }
//    
//    func startHosting(action: UIAlertAction!) {
//        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: mcSession)
//        mcAdvertiserAssistant.start()
//    }
//    
//    func joinSession(action: UIAlertAction!) {
//        let mcBrowser = MCBrowserViewController(serviceType: "hws-kb", session: mcSession)
//        mcBrowser.delegate = self
//        presentViewController(mcBrowser, animated: true, completion: nil)
//    }
//    
//    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveStream stream: \(stream), withName streamName: \(streamName), fromPeer peerID: \(peerID))")
//    }
//    
//    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
//        print("func session(session: \(session), didStartReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), withProgress progress: \(progress))")
//    }
//    
//    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
//        print("func session(session: \(session), didFinishReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), atURL localURL: \(localURL), withError error: \(error!))")
//    }
//    
//    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
//        print("func session(session: \(session), didReceiveData data: \(data), fromPeer peerID: \(peerID))")
//    }
//    
//    
//    
//    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerDidFinish(browserViewController: \(browserViewController))")
//    }
//    
//    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
//        dismissViewControllerAnimated(true, completion: nil)
//        print("browserViewControllerWasCancelled(browserViewController: \(browserViewController))")
//    }
//    
//    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
//        print("func session(session: \(session), peer peerID: \(peerID), didChangeState state: \(state))")
//        switch state {
//        case MCSessionState.Connected:
//            print("Connected: \(peerID.displayName)")
//            buttonCreateMultiplayerGame.enabled = true
//            self.view.setNeedsDisplay()
//            
//        case MCSessionState.Connecting:
//            print("Connecting: \(peerID.displayName)")
//            
//        case MCSessionState.NotConnected:
//            print("Not Connected: \(peerID.displayName)")
//        }
//    }
//    
//}