//
//  TableViewMenuController.swift
//  BlueTruco
//
//  Created by Sírius Gomes on 11/13/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import MultipeerConnectivity
import CoreData

class GameController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate {
    
    var table:Table?
    var cpuAI:CpuAI?
    static var gameMode:Int8? // 1 - Single Player    2 - Join Multiplayer     3 - Create Multiplayer
    
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var mcAdvertiserAssistant: MCAdvertiserAssistant!
    
    let width = UIScreen.mainScreen().bounds.size.width
    let height = UIScreen.mainScreen().bounds.size.height
    
    private var player1Card:Int?
    private var player2Card:Int?

    enum DeviceType {
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case iPadRetina
    }

    var gameType:GameTypeEnum?
    
    @IBOutlet weak var turnedCard: UIImageView!
    
    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    
    override func viewDidLoad() {

        super.viewDidLoad()
        print("viewDidLoad")
        
        if (GameController.gameMode == 1) {
            print("Single Player")
            gameType = .SinglePlayer
        }
        else if (GameController.gameMode == 2) {
            print("Join Multiplayer")
            gameType = .MultiPlayerClient
        }
        else if (GameController.gameMode == 3) {
            print("Create Multiplayer")
            gameType = .MultiPlayerServer
        }
        
        switch (gameType!) {
        case .SinglePlayer:
            table = Table(player1: Player(name: "Player1", playerType: 1, playerNumber: 1), player2: Player(name: "Player2", playerType: 2, playerNumber: 2), gameController: self, gameType:gameType!)
            cpuAI = CpuAI(table: table!)
            startSinglePlayer()
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "cpuPlayedCard:", name: "cpuPlayedCard", object: nil)
            break;
        case .MultiPlayerClient:
            peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
            mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .Required)
            mcSession.delegate = self
            joinSession(nil)
            break;
        case .MultiPlayerServer:
            peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
            mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .Required)
            mcSession.delegate = self
            startHosting(nil)
            break;
        }

        
    } 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        print("View will disappear..")
        switch (gameType!) {
        case .SinglePlayer:
            NSNotificationCenter.defaultCenter().removeObserver(self)
            NSNotificationCenter.defaultCenter().removeObserver(table!)
            NSNotificationCenter.defaultCenter().removeObserver(cpuAI!)
            break;
        case .MultiPlayerClient:
            NSNotificationCenter.defaultCenter().removeObserver(self)
            mcSession.disconnect()
//            NSNotificationCenter.defaultCenter().removeObserver(table!)
//            NSNotificationCenter.defaultCenter().removeObserver(cpuAI!)
            break;
        case .MultiPlayerServer:
            NSNotificationCenter.defaultCenter().removeObserver(self)
            //NSNotificationCenter.defaultCenter().removeObserver(table!)
            mcSession.disconnect()
//            NSNotificationCenter.defaultCenter().removeObserver(cpuAI!)
            break;
        }
        
    }

    func receivedPackage(notification: NSNotification) {
        let dict:NSDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(notification.object as! NSData)! as! NSDictionary
        print("Received Package", dict)
        
        // When a new game is starting - Client
        if (dict.valueForKey(Constants.PACKAGE_TYPE) as! String == Constants.SERVER_NEW_ROUND) {
            let player2 = Player(name: peerID.displayName, playerType: 1, playerNumber: 2)
            
            let arrayCards = dict.valueForKey(Constants.JSON_CARDS) as! NSArray
            
            var cards:Set<Card> = Set<Card>();
            cards.insert(Card(idsuit: arrayCards[0] as! String));
            cards.insert(Card(idsuit: arrayCards[1] as! String));
            cards.insert(Card(idsuit: arrayCards[2] as! String));
            
            let turnedCard = Card(idsuit: dict.valueForKey(Constants.JSON_CARD_TURNED) as! String)
            
            
            table = Table(player2: player2, gameController: self, gameType: gameType!, cards: cards, turnedCard: turnedCard)
            
            startJoinMultiPlayer(dict.valueForKey(Constants.JSON_SCORE_TEAM1) as! Int, scoreTeam2: dict.valueForKey(Constants.JSON_SCORE_TEAM2) as! Int)
        }
        // When the client plays a card. - Server
        else if (dict.valueForKey(Constants.PACKAGE_TYPE) as! String == Constants.CLIENT_PLAY_CARD) {
            
            let card = dict.valueForKey(Constants.JSON_CARD_PLAYED_NUMBER) as! Int
            let cardObj = table?.getCardPlayer(2, card: card)
            var userInfo = NSMutableDictionary()
            
            switch (card) {
            case 1:
                userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard1]
                break
            case 2:
                userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard2]
                break
            case 3:
                userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard3]
                break
            default:
                break
            }
            
            self.onSwipeOpponentPlayerCard(card, changeCardForReal: true)
            NSNotificationCenter.defaultCenter().postNotificationName("cardPlayed", object: cardObj, userInfo: userInfo as [NSObject : AnyObject])

        }
        // When the server plays a card. - Client
        else if (dict.valueForKey(Constants.PACKAGE_TYPE) as! String == Constants.SERVER_PLAY_CARD) {
            
            let card = dict.valueForKey(Constants.JSON_CARD_PLAYED_NUMBER) as! Int
            let cardType = Card(idsuit: (dict.valueForKey(Constants.JSON_CARD_PLAYED) as! String))
//            
//            switch (card) {
//            case 1:
//                self.opponentPlayerCard1.image = self.getCardImage(cardType, isTurnedCard: false)
//                break
//            case 2:
//                self.opponentPlayerCard2.image = self.getCardImage(cardType, isTurnedCard: false)
//                break
//            case 3:
//                self.opponentPlayerCard3.image = self.getCardImage(cardType, isTurnedCard: false)
//                break
//            default:
//                break
//            }
            
            player1Card = card
            
            self.onSwipeOpponentPlayerCard(card, changeCardForReal: false, cardImage: self.getCardImage(cardType, isTurnedCard: false))
        }
        // Cleans the screen - Client
        else if (dict.valueForKey(Constants.PACKAGE_TYPE) as! String == Constants.CLEAN_SCREEN) {
            print("Cleaning screen!")
            
            var player1CardImage:UIImageView?
            var player2CardImage:UIImageView?
            
            switch (player1Card!) {
            case 1:
                player1CardImage = self.opponentPlayerCard1
                break
            case 2:
                player1CardImage = self.opponentPlayerCard2
                break
            case 3:
                player1CardImage = self.opponentPlayerCard3
                break
            default:
                break
            }
            switch (player2Card!) {
            case 1:
                player2CardImage = self.localPlayerCard1
                break
            case 2:
                player2CardImage = self.localPlayerCard2
                break
            case 3:
                player2CardImage = self.localPlayerCard3
                break
            default:
                break
            }
            
            //PERFORM SELECTOR WASN'T WORKING HERE, I DON'T KNOW WHY.
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.75 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                player1CardImage!.hidden = true
                player2CardImage!.hidden = true
                self.view.setNeedsLayout()
            }
        }
    }
    
    func setLabelMessageText(text:String) {
        self.labelMessage.text = text
    }
    
    func setLabelPointsText(text:String) {
        self.labelPoints.text = text
    }
    
    func cpuPlayedCard(notification:NSNotification) {
        let card = notification.userInfo!["card"] as! Int!
    
        let cardObj = table?.getCardPlayer(2, card: card)
        var userInfo = NSMutableDictionary()
        
        switch (card) {
        case 1:
            userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard1]
            break
        case 2:
            userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard2]
            break
        case 3:
            userInfo = ["player": (table?.player2)!, "UIImageViewCard":opponentPlayerCard3]
            break
        default:
            break
        }
        
        self.onSwipeOpponentPlayerCard(card, changeCardForReal: true)
        NSNotificationCenter.defaultCenter().postNotificationName("cardPlayed", object: cardObj, userInfo: userInfo as [NSObject : AnyObject])
    }
    
    func changeCardImageForReal(cardNumber:Int) {
        dispatch_async(dispatch_get_main_queue(), {
            
            if (cardNumber == 1) {
                let card = (self.table?.getCardPlayer(2, card: 1))!
                self.opponentPlayerCard1.image = self.getCardImage(card, isTurnedCard: false)
                card.setCardImage(self.opponentPlayerCard1)
            }
            if (cardNumber == 2) {
                let card = (self.table?.getCardPlayer(2, card: 2))!
                self.opponentPlayerCard2.image = self.getCardImage(card, isTurnedCard: false)
                card.setCardImage(self.opponentPlayerCard1)
            }
            if (cardNumber == 3) {
                let card = (self.table?.getCardPlayer(2, card: 3))!
                self.opponentPlayerCard3.image = self.getCardImage(card, isTurnedCard: false)
                card.setCardImage(self.opponentPlayerCard1)
            }
            
             self.view.setNeedsLayout()
        })
    }
    
    func sendData(dataDictionary: NSMutableDictionary) {
        if mcSession.connectedPeers.count > 0 {
            do {
                let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(dataDictionary)
                try mcSession.sendData(data, toPeers: mcSession.connectedPeers, withMode: .Unreliable)

            } catch let error as NSError {
                let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .Alert)
                ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                presentViewController(ac, animated: true, completion: nil)
            }
            catch _ {
                print("Error sending data!")
            }
            
        }
    }
    
    func getDeviceSize() -> DeviceType {
        if (width == 375.0 && height == 667.0) {
            return DeviceType.iPhone6
        }
        else if (width == 414.0 && height == 736.0) {
            return DeviceType.iPhone6Plus
        }
        else if ((width == 1024.0 && height == 768.0) || height == 1024.0 && width == 768.0) {
            return DeviceType.iPadRetina
        }
        else if (width == 320.0 && height == 568.0) {
            return DeviceType.iPhone5
        }
        else {
            return DeviceType.iPhone5
        }
        
    }
    
    
    func startSinglePlayer() {
        
        table!.startGame()
        cpuAI!.reset()
        localPlayerCard1.hidden = false
        localPlayerCard2.hidden = false
        localPlayerCard3.hidden = false

        opponentPlayerCard1.hidden = false
        opponentPlayerCard2.hidden = false
        opponentPlayerCard3.hidden = false
       
        
        // Set the image of the cards.
        let localPlayerCardObject1 = (table?.getCardPlayer(1, card: 1))!
        let localPlayerCardObject2 = (table?.getCardPlayer(1, card: 2))!
        let localPlayerCardObject3 = (table?.getCardPlayer(1, card: 3))!
        let localPlayerCardImage1 = getCardImage(localPlayerCardObject1, isTurnedCard: false)
        let localPlayerCardImage2 = getCardImage(localPlayerCardObject2, isTurnedCard: false)
        let localPlayerCardImage3 = getCardImage(localPlayerCardObject3, isTurnedCard: false)
        localPlayerCard1.image = localPlayerCardImage1
        localPlayerCard2.image = localPlayerCardImage2
        localPlayerCard3.image = localPlayerCardImage3
        localPlayerCardObject1.setCardImage(localPlayerCard1)
        localPlayerCardObject2.setCardImage(localPlayerCard2)
        localPlayerCardObject3.setCardImage(localPlayerCard3)
        
        let genericCardImage = getCardImage(Card(id: CardValue.GENERIC, suit: CardSuit.CLUBS), isTurnedCard: false)
        opponentPlayerCard1.image = genericCardImage
        opponentPlayerCard2.image = genericCardImage
        opponentPlayerCard3.image = genericCardImage
        
        let turnedCardImage = getCardImage((table?.getTurnedCard())!, isTurnedCard: true)
        turnedCard.image = turnedCardImage
        
        print("Asking view to be redrawn")
        dispatch_async(dispatch_get_main_queue(), {
            self.view.setNeedsLayout()
            //self.view.setNeedsDisplay()
        })
    }
    
    func startJoinMultiPlayer(scoreTeam1:Int, scoreTeam2:Int) {
    
        dispatch_async(dispatch_get_main_queue(), {
        
            self.localPlayerCard1.hidden = false
            self.localPlayerCard2.hidden = false
            self.localPlayerCard3.hidden = false
            
            self.opponentPlayerCard1.hidden = false
            self.opponentPlayerCard2.hidden = false
            self.opponentPlayerCard3.hidden = false
            
            self.setLabelPointsText("Player1: \(scoreTeam1)\nPlayer2: \(scoreTeam2)")
            
            // Set the image of the cards.
            let localPlayerCardObject1 = (self.table?.getCardPlayer(2, card: 1))!
            let localPlayerCardObject2 = (self.table?.getCardPlayer(2, card: 2))!
            let localPlayerCardObject3 = (self.table?.getCardPlayer(2, card: 3))!
            let localPlayerCardImage1 = self.getCardImage(localPlayerCardObject1, isTurnedCard: false)
            let localPlayerCardImage2 = self.getCardImage(localPlayerCardObject2, isTurnedCard: false)
            let localPlayerCardImage3 = self.getCardImage(localPlayerCardObject3, isTurnedCard: false)
            self.localPlayerCard1.image = localPlayerCardImage1
            self.localPlayerCard2.image = localPlayerCardImage2
            self.localPlayerCard3.image = localPlayerCardImage3
            localPlayerCardObject1.setCardImage(self.localPlayerCard1)
            localPlayerCardObject2.setCardImage(self.localPlayerCard2)
            localPlayerCardObject3.setCardImage(self.localPlayerCard3)
            
            let genericCardImage = self.getCardImage(Card(id: CardValue.GENERIC, suit: CardSuit.CLUBS), isTurnedCard: false)

            self.opponentPlayerCard1.image = genericCardImage
            self.opponentPlayerCard2.image = genericCardImage
            self.opponentPlayerCard3.image = genericCardImage

            
            let turnedCardImage = self.getCardImage((self.table?.getTurnedCard())!, isTurnedCard: true)
            self.turnedCard.image = turnedCardImage
            
            self.view.setNeedsLayout()
            
        })
        
//        print("Asking view to be redrawn")
//        dispatch_async(dispatch_get_main_queue(), {
//            self.view.setNeedsLayout()
////            self.view.setNeedsDisplay()
//        })
    }
    
    func startMultiplayerPlayer(scoreTeam1:Int, scoreTeam2:Int) {
        
        table!.startGame()
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.localPlayerCard1.hidden = false
            self.localPlayerCard2.hidden = false
            self.localPlayerCard3.hidden = false
            
            self.opponentPlayerCard1.hidden = false
            self.opponentPlayerCard2.hidden = false
            self.opponentPlayerCard3.hidden = false
            
            // Set the image of the cards.
            let localPlayerCardObject1 = (self.table?.getCardPlayer(1, card: 1))!
            let localPlayerCardObject2 = (self.table?.getCardPlayer(1, card: 2))!
            let localPlayerCardObject3 = (self.table?.getCardPlayer(1, card: 3))!
            let localPlayerCardImage1 = self.getCardImage(localPlayerCardObject1, isTurnedCard: false)
            let localPlayerCardImage2 = self.getCardImage(localPlayerCardObject2, isTurnedCard: false)
            let localPlayerCardImage3 = self.getCardImage(localPlayerCardObject3, isTurnedCard: false)
            self.localPlayerCard1.image = localPlayerCardImage1
            self.localPlayerCard2.image = localPlayerCardImage2
            self.localPlayerCard3.image = localPlayerCardImage3
            localPlayerCardObject1.setCardImage(self.localPlayerCard1)
            localPlayerCardObject2.setCardImage(self.localPlayerCard2)
            localPlayerCardObject3.setCardImage(self.localPlayerCard3)
            
            let genericCardImage = self.getCardImage(Card(id: CardValue.GENERIC, suit: CardSuit.CLUBS), isTurnedCard: false)
            
            self.opponentPlayerCard1.image = genericCardImage
            self.opponentPlayerCard2.image = genericCardImage
            self.opponentPlayerCard3.image = genericCardImage
            
            
            let turnedCardImage = self.getCardImage((self.table?.getTurnedCard())!, isTurnedCard: true)
            self.turnedCard.image = turnedCardImage
            
            self.view.setNeedsLayout()
            
        })
        
        
        
        let initialGamePackage = NSMutableDictionary()
        initialGamePackage.setValue(Constants.SERVER_NEW_ROUND, forKey: Constants.PACKAGE_TYPE)
        initialGamePackage.setValue(table!.getTurnedCard().description(), forKey: Constants.JSON_CARD_TURNED)
        let cards = NSMutableArray()
        for card in (table!.player2?.getCards())! {
            cards.addObject(card.description())
        }
        initialGamePackage.setValue(cards, forKey: Constants.JSON_CARDS)
        initialGamePackage.setValue(scoreTeam1, forKey: Constants.JSON_SCORE_TEAM1)
        initialGamePackage.setValue(scoreTeam2, forKey: Constants.JSON_SCORE_TEAM2)
        
//        
//        print("Asking view to be redrawn")
//        dispatch_async(dispatch_get_main_queue(), {
//            self.view.setNeedsLayout()
////            self.view.setNeedsDisplay()
//        })

        print("Sending package:",initialGamePackage)
        sendData(initialGamePackage)

    }
    
    //MARK: Card swipe methods for local player
    @IBOutlet var localPlayerCard3: UIImageView!
    @IBOutlet var localPlayerCard2: UIImageView!
    @IBOutlet var localPlayerCard1: UIImageView!
    
    @IBAction func onSwipeLocalPlayerCard1(sender: AnyObject) {
        onSwipeLocalPlayerCard(1)
    }
    @IBAction func onSwipeLocalPlayerCard2(sender: AnyObject) {
        onSwipeLocalPlayerCard(2)
    }
    @IBAction func onSwipeLocalPlayerCard3(sender: AnyObject) {
        onSwipeLocalPlayerCard(3)
    }
    
    func onSwipeLocalPlayerCard(card:Int) {
        print("Swipe \(card) card")
        
        if (gameType == .MultiPlayerClient) {
            player2Card = card
        }
        
        var x:Int? = 0
        var y:Int? = 0
        switch(getDeviceSize()) {
        case .iPadRetina:
            x = 50 - (50 * (table?.getTurn())!) - 70
            y = -305
            break
        case .iPhone5:
            x = 50 - (50 * (table?.getTurn())!)
            y = -85
            break
        case .iPhone6:
            x = 50 - (50 * (table?.getTurn())!)
            y = -120
            break
        case .iPhone6Plus:
            x = 50 - (50 * (table?.getTurn())!)
            y = -160
            break
        }
        print("Swiping x \(x) and y \(y)")
        dispatch_async(dispatch_get_main_queue(), {
            UIImageView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                if (card == 1) {
                    self.localPlayerCard1.center = CGPoint(x: x!, y: y!)
                }
                else if (card == 2) {
                    self.localPlayerCard2.center = CGPoint(x: x!, y: y!)
                }
                else if (card == 3) {
                    self.localPlayerCard3.center = CGPoint(x: x!, y: y!)
                }
            }, completion: nil)
            self.view.setNeedsLayout()
        })
        
        if (gameType == .SinglePlayer || gameType == .MultiPlayerServer) {
            if (card == 1) {
                NSNotificationCenter.defaultCenter().postNotificationName("cardPlayed", object: (table?.getCardPlayer(1, card: card))!, userInfo: ["player": (table?.player1)!, "UIImageViewCard":localPlayerCard1])
            }
            else if (card == 2) {
                NSNotificationCenter.defaultCenter().postNotificationName("cardPlayed", object: (table?.getCardPlayer(1, card: card))!, userInfo: ["player": (table?.player1)!, "UIImageViewCard":localPlayerCard2])
            }
            else if (card == 3) {
                NSNotificationCenter.defaultCenter().postNotificationName("cardPlayed", object: (table?.getCardPlayer(1, card: card))!, userInfo: ["player": (table?.player1)!, "UIImageViewCard":localPlayerCard3])
            }
            
        }
        if (gameType == .MultiPlayerServer || gameType == .MultiPlayerClient) {
            let cardNumber = card
            var playerNumber = 0
            
            let test = NSMutableDictionary()
            
            if (gameType == .MultiPlayerServer) {
                test.setValue(Constants.SERVER_PLAY_CARD, forKey: Constants.PACKAGE_TYPE)
                playerNumber = 1
            }
            else {
                test.setValue(Constants.CLIENT_PLAY_CARD, forKey: Constants.PACKAGE_TYPE)
                playerNumber = 2
            }
            test.setValue(cardNumber, forKey: Constants.JSON_CARD_PLAYED_NUMBER)
            test.setValue((table?.getCardPlayer(playerNumber, card: cardNumber))!.description(), forKey: Constants.JSON_CARD_PLAYED)
            
            print("Sending package:", test)
            sendData(test)
            
        }
    }
    
    //MARK: Card swipe methods for remote/cpu player
    @IBOutlet var opponentPlayerCard1: UIImageView!
    @IBOutlet var opponentPlayerCard2: UIImageView!
    @IBOutlet var opponentPlayerCard3: UIImageView!
    
    func onSwipeOpponentPlayerCard1() {
        onSwipeOpponentPlayerCard(1,changeCardForReal: true)
    }
    func onSwipeOpponentPlayerCard2() {
        onSwipeOpponentPlayerCard(2,changeCardForReal: true)
    }
    func onSwipeOpponentPlayerCard3() {
        onSwipeOpponentPlayerCard(3,changeCardForReal: true)
    }
    
    func onSwipeOpponentPlayerCard(card: Int, changeCardForReal: Bool, cardImage:UIImage?=nil) {
        print("Opponent \(card) card")
        if (changeCardForReal) {
            changeCardImageForReal(card)
        }
        var x:Int? = 0
        var y:Int? = 0
        switch(getDeviceSize()) {
        case .iPadRetina:
            x = 50 - (50 * (table?.getTurn())!) - 70
            y = 400
            break
        case .iPhone5:
            x = 50 - (50 * (table?.getTurn())!)
            y = 200
            break
        case .iPhone6:
            x = 50 - (50 * (table?.getTurn())!)
            y = 250
            break
        case .iPhone6Plus:
            x = 50 - (50 * (table?.getTurn())!)
            y = 300
            break
        }
        
        print("Swiping2 x \(x) and y \(y)")
        let cardObj = (self.table?.getCardPlayer(2, card: card))!
        
        
        dispatch_async(dispatch_get_main_queue(), {
            UIImageView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                if (card == 1) {
                    self.opponentPlayerCard1.center = CGPoint(x: x!, y: y!)
                    if (changeCardForReal) {
                        self.opponentPlayerCard1.image = self.getCardImage(cardObj, isTurnedCard: false)
                        cardObj.setCardImage(self.opponentPlayerCard1)
                    }
                    else if (cardImage != nil) {
                        self.opponentPlayerCard1.image = cardImage
                        cardObj.setCardImage(self.opponentPlayerCard1)
                    }
                }
                else if (card == 2) {
                    self.opponentPlayerCard2.center = CGPoint(x: x!, y: y!)
                    if (changeCardForReal) {
                        self.opponentPlayerCard2.image = self.getCardImage(cardObj, isTurnedCard: false)
                        cardObj.setCardImage(self.opponentPlayerCard2)
                    }
                    else if (cardImage != nil) {
                        self.opponentPlayerCard2.image = cardImage
                        cardObj.setCardImage(self.opponentPlayerCard2)
                    }
                }
                else if (card == 3) {
                    self.opponentPlayerCard3.center = CGPoint(x: x!, y: y!)
                    if (changeCardForReal) {
                        self.opponentPlayerCard3.image = self.getCardImage(cardObj, isTurnedCard: false)
                        cardObj.setCardImage(self.opponentPlayerCard3)
                    }
                    else if (cardImage != nil) {
                        self.opponentPlayerCard3.image = cardImage
                        cardObj.setCardImage(self.opponentPlayerCard3)
                    }
                }
                }, completion: { (completed:Bool) -> Void in
                    //self.opponentPlayerCard3.setNeedsLayout()
                    //print("Updating card \(card)", completed)
            })
            self.view.setNeedsLayout()
        })
        
    }
    
    
    //MARK: UI elements and effects generators
    
    /**
    Function that will move the card smoothly
    */
    func moveImage(view: UIImageView){
        let toPoint: CGPoint = CGPointMake(55.0, 39.0)
        let fromPoint : CGPoint = CGPointZero
        
        let movement = CABasicAnimation(keyPath: "movement")
        movement.additive = true
        movement.fromValue =  NSValue(CGPoint: fromPoint)
        movement.toValue =  NSValue(CGPoint: toPoint)
        movement.duration = 0.3
        
        view.layer.addAnimation(movement, forKey: "move")
    }
    
    /**
     Function to cut the image cards.png to show just one card at a time.
     */
    func getCardImage(card:Card, isTurnedCard:Bool?) -> UIImage {
        
        let image = UIImage(named: "cards.png")
        
        let contextImage: UIImage = UIImage(CGImage: image!.CGImage!)
        
        var x:Float = -1
        var y:Float = -1
        
        switch (card.getSuit()) {
        case .SPADES:
            y = 4
            break;
        case .HEARTS:
            y = 3
            break;
        case .DIAMONDS:
            y = 2
            break;
        case .CLUBS:
            y = 1
            break;
        }
        
        switch (card.getId()) {
        case .ACE:
            x = 1
            break;
        case .TWO:
            x = 2
            break;
        case .THREE:
            x = 3
            break;
        case .FOUR:
            x = 4
            break;
        case .FIVE:
            x = 5
            break;
        case .SIX:
            x = 6
            break;
        case .SEVEN:
            x = 7
            break;
        case .QUEEN:
            x = 12
            break;
        case .JACK:
            x = 11
            break;
        case .KING:
            x = 13
            break;
        case .GENERIC:
            x = 3;
            y = 5;
        }
        
        let posX: CGFloat = CGFloat((x*98.5)-98.5)
        let posY: CGFloat = CGFloat((y*153)-153)
        let cgwidth: CGFloat = CGFloat(98.5)
        let cgheight: CGFloat = CGFloat(153)
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)!
        
        if (isTurnedCard!) {
            // Create a new image based on the imageRef and scale it to 50% of the original size.
            return UIImage(CGImage: imageRef, scale: 1.5, orientation: image!.imageOrientation)
        }
        else {
            // Create a new image based on the imageRef and rotate back to the original orientation
            return UIImage(CGImage: imageRef, scale: image!.scale, orientation: image!.imageOrientation)
        }
    }
    
    //MARK: Functions for MultipeerConnection
    func startHosting(action: UIAlertAction!) {
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: mcSession)
        mcAdvertiserAssistant.start()
    }
    
    func joinSession(action: UIAlertAction!) {
        let mcBrowser = MCBrowserViewController(serviceType: "hws-kb", session: mcSession)
        mcBrowser.delegate = self
        presentViewController(mcBrowser, animated: true, completion: nil)
    }
    
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        print("1func session(session: \(session), didReceiveStream stream: \(stream), withName streamName: \(streamName), fromPeer peerID: \(peerID))")
    }
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        print("2func session(session: \(session), didStartReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), withProgress progress: \(progress))")
    }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        print("3func session(session: \(session), didFinishReceivingResourceWithName resourceName: \(resourceName), fromPeer peerID: \(peerID), atURL localURL: \(localURL), withError error: \(error!))")
    }
    
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        //print("4func session(session: \(session), didReceiveData data: \(data), fromPeer peerID: \(peerID))")
        print("Session", "Package received!")
        NSNotificationCenter.defaultCenter().postNotificationName("receivedPackage", object: data, userInfo: nil)
    }
    
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
        dismissViewControllerAnimated(true, completion: nil)
        //print("5browserViewControllerDidFinish(browserViewController: \(browserViewController))")
    }
    
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
        dismissViewControllerAnimated(true, completion: nil)
        //print("6browserViewControllerWasCancelled(browserViewController: \(browserViewController))")
    }
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        //print("7func session(session: \(session), peer peerID: \(peerID), didChangeState state: \(state))")
        switch state {
        case MCSessionState.Connected:
            print("Connected: \(peerID.displayName)")
            if (gameType == .MultiPlayerServer) {
                table = Table(player1: Player(name: peerID.displayName, playerType: 1, playerNumber: 1), player2: Player(name: mcSession.connectedPeers[0].displayName, playerType: 1, playerNumber: 2), gameController: self, gameType:gameType!)
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedPackage:", name: "receivedPackage", object: nil)
                startMultiplayerPlayer(0,scoreTeam2: 0)
            }
            else if (gameType == .MultiPlayerClient) {
                // update view somehow..
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedPackage:", name: "receivedPackage", object: nil)
            }
            self.view.setNeedsDisplay()
            
        case MCSessionState.Connecting:
            print("Connecting: \(peerID.displayName)")
            
        case MCSessionState.NotConnected:
            print("Not Connected: \(peerID.displayName)")
        }
    }
    
}