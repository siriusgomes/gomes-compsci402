//
//  TableeViewController.swift
//  Project1
//
//  Created by Sírius Gomes on 10/19/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickButtonRefreshJson(sender: AnyObject) {
        getJsonAndAddCoreData()
    }
    
    
    func getJsonAndAddCoreData() {
        // Create the URL of the JSON to be requested.
        let request = NSURLRequest(URL: NSURL(string: "http://zstudiolabs.com/labs/compsci402/buildings.json")!)
        // Execute the request
        JSONDownloader.httpGet(request) { (data, error) -> Void in
            if error != nil {
                print(error)
            } else {
                // Call the function to add the pins in the mapView.
                //print(data)
                print("TableViewDownloadedData: ", data)
                self.addAnnotationsToCoreData(data)
            }
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // HERE I HAVE TO UPDATE THE TABLE VIEW SOMEHOW..
        //self.mapView.addAnnotations(Location.getLocations())
        
    }
    
    // Function that will add the pins from the json file to the mapView.
    func addAnnotationsToCoreData(data: AnyObject) {
        do {
            // First convert the string read from the server to an NSData.
            let jsonData = data.dataUsingEncoding(NSUTF8StringEncoding)!
            // Then we create a JSON from this NSData.
            let jsonLocations = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.AllowFragments)
            // Convert the AnyObject to an iterable NSArray
            let arrayJsonLocations = jsonLocations as! NSArray
            
            // Iterate over the array, creating instances of Location, appending them on this array.
            for jsonLocation in arrayJsonLocations {
                Location.saveLocation(Location.fromJSON(try NSJSONSerialization.dataWithJSONObject(jsonLocation, options: NSJSONWritingOptions.PrettyPrinted))!)
            }
            

            tableView.reloadData();
            // HERE I HAVE TO UPDATE THE TABLE VIEW SOMEHOW..
            
        } catch _ {
            // Error
        }
    }
    
    

    
    // Set number of rows.
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Location.getLocations().count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        print("tableView reloading cell..")
        
        // I'm using this prototype cell that I create that will contain the image, the label and a detail label. Exactly what I needed.
        let cell = tableView.dequeueReusableCellWithIdentifier("SiriusCell", forIndexPath:indexPath)
        
        let location = Location.getLocations()[indexPath.row] 
        cell.textLabel?.text = location.title
        cell.detailTextLabel?.text = location.subtitle
        cell.imageView?.image = location.image
        
        cell.accessoryType = .None
        return cell
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
