//
//  Location.swift
//  Project1
//
//  Created by Sírius Gomes on 9/21/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation
import MapKit
import CoreData

class Location: NSObject, MKAnnotation {
    
    private static var arrayLocations = [NSManagedObject]()
    static var current_latitude = 0.0
    static var current_longitude = 0.0

    var title: String?
    var subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let image: UIImage?
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D, image: UIImage?) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.image = image
        super.init()
    }
    
    func getTitle() -> String {
        return self.title!
    }
    
    func getSubtitle() -> String {
        return self.subtitle!
    }
    
    func getCoordinate() -> CLLocationCoordinate2D {
        return self.coordinate
    }
    
    /*
        Function to create an instance of Location with the parameter data, that is a JSON.
     */
    static func fromJSON(data: NSData) -> Location? {
        
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
            let coordinate = CLLocationCoordinate2D(latitude: json["location"]!!["latitude"] as! Double, longitude: json["location"]!!["longitude"] as! Double)
            
            return Location(title: json["name"] as! String, subtitle: json["description"] as! String, coordinate: coordinate, image: nil)

        } catch _ {
        // Error
        }

        return nil;
    }
    
    
    /*
        Function to create an instance of Location with the parameter building, that comes from the coredata models building and coordinates.
    */
    static func fromBuilding(building: Building) ->  Location {
        let coordinate = CLLocationCoordinate2D(latitude: building.coordinate?.latitude as Double!, longitude: building.coordinate?.longitude as Double!)
        if (building.photo != nil) {
            return Location(title: building.name!, subtitle: building.information!, coordinate: coordinate, image: UIImage(data: building.photo! as NSData))
        }
        else {
            return Location(title: building.name!, subtitle: building.information!, coordinate: coordinate, image: nil)
        }
    }
    
    static func getCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: current_latitude, longitude: current_longitude)
    }
    
    
    /* 
        Function that returns an array of all the locations that are on the DB. It will be used to populate the map.
    */
    static func getLocations() -> [Location] {
        var arrayLocations = [Location]()
        
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Building")
        

        
        //3
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
            
            for building in results {
                arrayLocations.append(fromBuilding(building as! Building))
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        //print(arrayLocations)
        
        return arrayLocations
        
    }
    
    
    /*
        Function to return the size of the array.
    */
    static func getLocationsSize() -> Int {
        return getLocations().count
    }
    
    
    /*
        Function that saves a location on the DB. 
    */
    static func saveLocation(location: Location) {
        
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        
        //2
        let building = NSEntityDescription.insertNewObjectForEntityForName("Building", inManagedObjectContext: context) as! Building
        building.name = location.getTitle()
        building.information = location.getSubtitle()
        if (location.image != nil) {
            building.photo = UIImagePNGRepresentation(location.image!)
        }
        let coordinate = NSEntityDescription.insertNewObjectForEntityForName("Coordinate", inManagedObjectContext: context) as! Coordinate
        coordinate.latitude = location.getCoordinate().latitude
        coordinate.longitude = location.getCoordinate().longitude
        
        building.coordinate = coordinate
        
        do {
            //appDelegate.saveContext()
            try context.save()
        }
        catch {
            context.deleteObject(building)
            context.deleteObject(coordinate)
        }
        
    }
}