//
//  Map ViewController.swift
//  Project1
//
//  Created by Sírius Gomes on 9/21/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapViewController: UIViewController {

    @IBOutlet weak var buttonRefreshJson: UIBarButtonItem!
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Boise coordinate.
        let initialLocation = CLLocation(latitude:  43.601869, longitude: -116.198573)
        // Size of square to zoom
        let regionRadius: CLLocationDistance = 2000
        // Centralizing the map over BSU.
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        doAction()
        
        // Showing user current location.
        self.checkLocationAuthorizationStatus()
        
    }
    
    
    func doAction() {
        getJsonAndAddPinsMap()
        
        // If there's no buildings after the request:
        if (Location.getLocationsSize() == 0) {
            let uiAlert = UIAlertController(title: "Alert", message: "There is no buildings", preferredStyle: UIAlertControllerStyle.Alert)
            uiAlert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(uiAlert, animated: true, completion: nil)
        }
        
        // Add the big array on the mapView.
        self.mapView.addAnnotations(Location.getLocations())
    }
    
    // Call the function that will download the json and add the pins in the map when the user clicks in the refresh button.
    @IBAction func onClickButtonRefreshJson(sender: AnyObject) {
        doAction()
    }
    
    func getJsonAndAddPinsMap() {
        // Create the URL of the JSON to be requested.
        let request = NSURLRequest(URL: NSURL(string: "http://zstudiolabs.com/labs/compsci402/buildings.json")!)
        // Execute the request
        JSONDownloader.httpGet(request) { (data, error) -> Void in
            if error != nil {
                print(error)
            } else {
                // Call the function to add the pins in the mapView.
                print("MapViewDownloadedData: ", data)
                self.addAnnotationsToMapView(data)
            }
        }

    }
    
    override func viewDidAppear(animated: Bool) {
      
        // Add the array of locations on the mapView. It will be called when the view appears, when the user save a location, it will be saved and will be added to the map here.
        self.mapView.addAnnotations(Location.getLocations())
        
    }
    
    // Function that will add the pins from the json file to the mapView.
    func addAnnotationsToMapView(data: AnyObject) {
        do {
            // First convert the string read from the server to an NSData.
            let jsonData = data.dataUsingEncoding(NSUTF8StringEncoding)!
            // Then we create a JSON from this NSData.
            let jsonLocations = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.AllowFragments)
            // Convert the AnyObject to an iterable NSArray
            let arrayJsonLocations = jsonLocations as! NSArray
            
            // Iterate over the array, creating instances of Location, appending them on this array.
            for jsonLocation in arrayJsonLocations {
               // Location.arrayLocations.append(Location.fromJSON(try NSJSONSerialization.dataWithJSONObject(jsonLocation, options: NSJSONWritingOptions.PrettyPrinted))!)
                
                Location.saveLocation(Location.fromJSON(try NSJSONSerialization.dataWithJSONObject(jsonLocation, options: NSJSONWritingOptions.PrettyPrinted))!)
            }
            
        } catch _ {
            // Error
        }
    }
    
    var locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
}

