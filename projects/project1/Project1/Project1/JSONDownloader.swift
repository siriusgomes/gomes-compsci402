//
//  JSONDownloader.swift
//  Project1
//
//  Created by Sírius Gomes on 9/21/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//

import Foundation


class JSONDownloader {
    
    // Function that will make a GET in the url request, returning an (?,?) where the first parameter is the answer, and the second is an error if it occurs.
    // I got this code from: https://medium.com/swift-programming/learn-nsurlsession-using-swift-ebd80205f87c
    static func httpGet(request: NSURLRequest!, callback: (String, String?) -> Void) {
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            if error != nil {
                callback("", error!.localizedDescription)
            } else {
                callback(NSString(data: data!, encoding: NSASCIIStringEncoding) as! String, nil)
            }
        }
        task.resume()
    }
    
    
    
}