//
//  FormViewController.swift
//  Project1
//
//  Created by Sírius Gomes on 9/22/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//


import UIKit
import MapKit

class FormViewController: UIViewController, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var buttonSave: UIBarButtonItem!
    
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldDescription: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    let locationManager = CLLocationManager()
    var didUpdatedLocation:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    
    // Function that will set the current location in the "singleton" Location.
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        Location.current_latitude = userLocation.coordinate.latitude
        Location.current_longitude = userLocation.coordinate.longitude
        didUpdatedLocation = true
    }
    
    // Function for button save
    @IBAction func onClickButtonSave(sender: AnyObject) {
        
        if (!textFieldDescription.text!.isEmpty && !textFieldTitle.text!.isEmpty && didUpdatedLocation) {
            // Create the new location with data that the user inserted in the view, and with the location that the GPS is getting.
            
            if (imageView.image == nil) {
                let newLocation = Location(title: textFieldTitle.text!, subtitle: textFieldDescription.text!, coordinate: Location.getCoordinate2D(), image: nil)
                
                // Add this new location in the array of locations.
                Location.saveLocation(newLocation)
            }
            else {
                let newLocation = Location(title: textFieldTitle.text!, subtitle: textFieldDescription.text!, coordinate: Location.getCoordinate2D(), image: imageView.image!)
                
                // Add this new location in the array of locations.
                Location.saveLocation(newLocation)
            }
        
            // Close this View and return to the last view in the stack.
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    @IBAction func onClickButtonAddImage(sender: AnyObject) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else {
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        //imagePickerController.allowsEditing = true
        self.presentViewController(imagePickerController, animated: true, completion: nil)
        

    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imageView.image = image
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

