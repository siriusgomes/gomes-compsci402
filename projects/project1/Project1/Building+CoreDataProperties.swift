//
//  Building+CoreDataProperties.swift
//  Project1
//
//  Created by Sírius Gomes on 10/27/15.
//  Copyright © 2015 Sírius Gomes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Building {

    @NSManaged var information: String?
    @NSManaged var name: String?
    @NSManaged var photo: NSData?
    @NSManaged var coordinate: Coordinate?

}
